// Firebase App (the core Firebase SDK) is always required and must be listed first
import * as firebase from "firebase/app";

// Add the Firebase products that you want to use
import "firebase/auth";
import "firebase/database";

// PUT YOUR OWN FIREBASE CONFIGURATION HERE
const firebaseConfig = {
  apiKey: "AIzaSyCJJeYn9iUPRq5zyg1aCj587iO3nv8ipGo",
  authDomain: "esp-chat-cifrado.firebaseapp.com",
  databaseURL: "https://esp-chat-cifrado-default-rtdb.firebaseio.com",
  projectId: "esp-chat-cifrado",
  storageBucket: "esp-chat-cifrado.appspot.com",
  messagingSenderId: "381488615340",
  appId: "1:381488615340:web:78e62dec13fabc1cbef409"
};

// Initialize Firebase
let firebaseApp = firebase.initializeApp(firebaseConfig);
let firebaseAuth = firebaseApp.auth()
let firebaseDb = firebaseApp.database()

export { firebaseAuth, firebaseDb }