import { JSEncrypt } from 'jsencrypt'

const privateKey = `-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAxOxTIz9tLDR5KBpFxRFPND36bskuelP/gQjj0LypixBaJKz2
aA08kct70ttPmDH2mWbjyAhNmEjpombgrkwd9PLoa3XsMTEQYd74XAZykqKbbpEP
e0FAkcyUqQpsjq83JHkAdkpiGBtBDxyWLoET4gagk93owkLfZu9vRed4HiIzGV29
6XF1e4Gc/v6OEyrM/5RXOMYmI6vHJ4f6Zz/8MfYzLKeVZVuIIvOVL9f4xojFRpPH
RYYaoFfuc78cOQvykLhykkjQyrMkYkmDQRWw59HYL+ff1LbLD2gBEf9fqE3ABedH
zOhAirEACl5dsn+tWDUY0ukAcSbPAIen6fKuKQIDAQABAoIBADim3EwZpSjAd9KH
rGbDUgvvgKp6ijp7C9htXEzi2cu+CpDeiYzpsfEnnxnMKhvY097R8NnNObWD1GL6
Qc0i7MNZVnAD3s1QFixx3vVqjW+OBDVM2i1VWmQXfPUcoJpz4eNGRFHDmHDi9laI
nNQIIlY2HeAsSRgJ8QdF/rcXnkDch+7gl7XQKEFz0E1f5arfJIPpUcgmjpw6mOqq
HaP+WVaJtkTuVoe+S2HLOHgqCXYwCnCVuKfVqo6zBJ3ZoncIFofPoWYFYxft9o4i
71OBTzwbGdV7Itu8Ra7vMdAhEX2YUozr/cRBPpBv24xXRXVG1GThKu8MqKlDhrQq
Qy2UWXkCgYEA+lPpwPNAHGL/+5LHTPCCQ9GEfW6z0SMmnXL+uwRg84713CV3yQm5
gr25hmEBuHWuUlBknjRe0jrWgvoteIU6dQN0Qy7TStttiDlbWhrfz2KHV3Z0P8JQ
kWxj//Xa14OrPs3NgKtI1/tDxscs4Y51UKiAEhH+Y1l82K/69hA4yO8CgYEAyWKg
BvoM/DNBDov1Ywr3n2EXIfowANW0spTpl9T7JjmKSBxVQPcCMUdD91p+WipTXcxx
dWieYkVwpYzDyto1nuabh5iXwHcXkuzsIZ797fBc3HMatjgoc/qOB0GiG0P5s2ns
t3V+dQCGnWJUhNm14lM/K/RUEIG0qX6Q4rvHimcCgYA+1j+QKr+GIKfHO8IBo0Hf
ykEqx1YmAl/YGByvT1sS6Gj4+QLHmURS+FMQ01r+NbQrf3iyacz/krt0J16wqSjS
ZKqTjraFTO2Cy3sN9D8ILOByaQ6nTpyw1HeJsepcPPYIjtquGNDxPmZ2CFtQx1TV
iWaUoqWrxLPdJNenWs0WSwKBgQDJRYTi9C7eMOuoekE6R8/MvuC72Kz5aJ08jpAY
a4jHX2LRR1/9meHbFnrbUNpPi1XevWH4EiOZ3AUFsHZO22AT/C8dimjAt0YcViZ6
Q8lOcZidy3y6+c2UfKpOnk5CKeH6ardndEp67Zec3JXUYTLjjIfqijsqdDf33sM3
mK2JqQKBgQDbNLjZFA2ekURZ6D0xoAq9HIdD14dajGRMjQTIY2zVI334WSu5vrnG
N47woy2Iy1p19r481rWfkcV3vI8BOScxlY5XsE/cH4G96dRuvPscY8wFbaLDbPfg
/Y/lqG4J6WtMpNXg4vL99zjgRXuVmdgyeWQwMntm2JRJh6K6IJ2KvQ==
-----END RSA PRIVATE KEY-----`

const publicKey = `-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxOxTIz9tLDR5KBpFxRFP
ND36bskuelP/gQjj0LypixBaJKz2aA08kct70ttPmDH2mWbjyAhNmEjpombgrkwd
9PLoa3XsMTEQYd74XAZykqKbbpEPe0FAkcyUqQpsjq83JHkAdkpiGBtBDxyWLoET
4gagk93owkLfZu9vRed4HiIzGV296XF1e4Gc/v6OEyrM/5RXOMYmI6vHJ4f6Zz/8
MfYzLKeVZVuIIvOVL9f4xojFRpPHRYYaoFfuc78cOQvykLhykkjQyrMkYkmDQRWw
59HYL+ff1LbLD2gBEf9fqE3ABedHzOhAirEACl5dsn+tWDUY0ukAcSbPAIen6fKu
KQIDAQAB
-----END PUBLIC KEY-----`


const encryptor = new JSEncrypt()
encryptor.setPublicKey(publicKey)

const decryptor = new JSEncrypt()
decryptor.setPrivateKey(privateKey)

export class RcaUtil {
    static encrypt(value) {
        return encryptor.encrypt(value)
    }

    static decrypt(value) {
        return decryptor.decrypt(value)
    }
}